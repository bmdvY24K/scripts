#!/bin/bash

DOCROOT='/srv/krMKg1Gs/0/bmdvY24K/ipmp/public'                                      
FILES="$(find $DOCROOT -type f )"

ret=0

echo "Start: [$(date)]"

#sed -i 's#ipns/ipfs.ngocn2.org##g' $DOCROOT/index.html
#ret=$?

for FILE in $FILES; do
        #echo ">>> "$FILE
        sed -i 's#/ipns/ipfs.ngocn2.org##g' $FILE
        #echo $?
        ret_new=$?
        if [[ ${ret_new} -ne 0 ]]; then
                ret="$ret_new"
        fi
done

echo "End: $ret [$(date)]"
exit



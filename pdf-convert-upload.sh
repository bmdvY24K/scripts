#!/bin/bash

## pdf-convert-upload.sh: 
## 1. Grab the latest updated file(article) name (from git commit);
## 2. Convert this article from its webpage to PDF; 
## 3. Upload it to IPFS (got IPFS hash) and put it to public/ for download.

# Using wkhtmltopdf
WKPDF="/usr/bin/wkhtmltopdf"
WKIMG="/usr/bin/wkhtmltoimage"

DOMAIN="https://ipfs.ngocn2.org"

IPFS_PATH="/srv/go-ipfs/ipfsrepo"

# This script should be run in the git repo.
GIT_REPO="/app/krMKg1Gs/0/bmdvY24K/ipmp"

# TODO: can ONLY get one file name in the latest commit.
cd ${GIT_REPO}
FILE=$(git show -10 --name-only |grep 'src/.*md$' |head -1)

if [ ! -f "$FILE" ]; then
    echo ">>> No .md file found in the recent 10 commits. Did you update and commit some md files?"
    exit 1
fi

ARTICLE=$(echo ${FILE##*/} | sed 's/\.md//')

OUTPUT="./public/${ARTICLE}.pdf"
OUTPUT2="./public/${ARTICLE}.png"

URL="${DOMAIN}/article/${ARTICLE}/"
#echo $URL

echo ">>> Converting to PDF"
$WKPDF $URL $OUTPUT
$WKIMG --quality 50 $URL $OUTPUT2

echo ">>> PDF file: "
ls -l $OUTPUT
ls -l $OUTPUT2

############## UPLOAD to IPFS ####################

# > ./public/ipfs-pdf.txt
PDF_HASH=$(/srv/go-ipfs/ipfs add -q ${OUTPUT})
IMG_HASH=$(/srv/go-ipfs/ipfs add -q ${OUTPUT2})

echo -e "\n===========================================================\n\nYou have PDF/PNG version of the webpage here:  
>>> You can visit it at: 
${DOMAIN}/${ARTICLE}.pdf
${DOMAIN}/${ARTICLE}.png

\nAND you can find this PDF/PNG on IPFS!\n
IPFS hash: 
${PDF_HASH} 
${IMG_HASH} \n
You can check these  from any IPFS gateways, e.g. 
https://nftstorage.link/ipfs/${PDF_HASH} \n
https://nftstorage.link/ipfs/${IMG_HASH} \n
There are a lot of IPFS gateways to choose: https://ipfs.github.io/public-gateway-checker/ \n
Enjoy!" |tee -a ./public/ipfs-pdf.txt

exit
